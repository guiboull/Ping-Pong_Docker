# Raczkiewicz Pavel & Boulet Guillaume

from flask import Flask, jsonify
from multiprocessing import Value

app = Flask(__name__)


counter = Value('i', 0)

@app.route('/ping')
def ping():
    with counter.get_lock():
       counter.value += 1
    return jsonify(message="pong")

@app.route('/count')
def count():
	return jsonify(pingCount=counter.value)

	
if __name__ == "__main__":
	app.run(debug=True, host='0.0.0.0')